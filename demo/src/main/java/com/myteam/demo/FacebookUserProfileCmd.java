package com.myteam.demo;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.executor.Command;
import org.kie.api.executor.CommandContext;
import org.kie.api.executor.ExecutionResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;


public class FacebookUserProfileCmd implements Command{
    
    private static final Logger logger = LoggerFactory.getLogger(FacebookUserProfileCmd.class);
    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
    
    private final static Gson gson = new Gson();
    
    public ExecutionResults execute(CommandContext ctx) throws Exception {
        logger.info("Command executed on executor with data {}", ctx.getData());
        WorkItem workItem = (WorkItem) ctx.getData("workItem");
        User user = (User) workItem.getParameter("_user");
        
        String psid = (String) workItem.getParameter("FBPageScopeId");
        String fbPageToken = (String) workItem.getParameter("FBPageAccessToken");
        
        String res = fbProfile(psid, fbPageToken);
		

		User userProfile = gson.fromJson(res, User.class);
        
        if (user == null) {
            logger.info("User is null");
            user = userProfile;
            user.setId(psid);
            
        } else {
            user.setUpdated(new java.util.Date());
        }
        
        ExecutionResults executionResults = new ExecutionResults();
        executionResults.setData("user_", user);
        return executionResults;
    }
    
    private String fbProfile(final String psid, String fbPageToken) throws Exception {

		HttpURLConnection connection = null;

		try {

			String qs = "access_token=" + URLEncoder.encode(fbPageToken, "UTF-8");

			final URL url = new URL("https://graph.facebook.com/v2.6/" + psid
					+ "?fields=first_name,last_name,profile_pic,locale,timezone,gender,last_ad_referral&" + qs);

			logger.info("Request URL: " + url.toString());

			// if (configuration.getProxy() != null) {
			// connection = (HttpURLConnection)
			// url.openConnection(configuration.getProxy());
			// } else {
			connection = (HttpURLConnection) url.openConnection();
			// }

			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.addRequestProperty("Content-Type", "application/json; charset=utf-8");

			connection.connect();

			final InputStream inputStream = new BufferedInputStream(connection.getInputStream());
			final String response = IOUtils.readAll(inputStream, DEFAULT_CHARSET);
			inputStream.close();
			logger.info("Response: " + response);
			return response;
		} catch (final IOException e) {
			if (connection != null) {
				try {
					final InputStream errorStream = connection.getErrorStream();
					if (errorStream != null) {
						final String errorString = IOUtils.readAll(errorStream, DEFAULT_CHARSET);
						logger.info(errorString);
						return errorString;
					} else {
						throw new Exception("Can't connect to the facebook service.", e);
					}
				} catch (final IOException ex) {
					logger.warn("Can't read error response", ex);
				}
			}
			logger.error(
					"Can't make request to the Facebook service. Please, check connection settings and API access token.",
					e);
			throw new Exception(
					"Can't make request to the Facebook service. Please, check connection settings and API access token.",
					e);

		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}
}

