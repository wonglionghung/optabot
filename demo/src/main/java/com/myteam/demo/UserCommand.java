package com.myteam.demo;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.executor.Command;
import org.kie.api.executor.CommandContext;
import org.kie.api.executor.ExecutionResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.myteam.demo.User;


public class UserCommand implements Command{
    
    private static final Logger logger = LoggerFactory.getLogger(UserCommand.class);

    public ExecutionResults execute(CommandContext ctx) {
        logger.info("Command executed on executor with data {}", ctx.getData());
        WorkItem workItem = (WorkItem) ctx.getData("workItem");
        User user = (User) workItem.getParameter("UserIn");
        user.setFirstName(user.getFirstName() + "456");
        ExecutionResults executionResults = new ExecutionResults();
        executionResults.setData("UserOut", user);
        return executionResults;
    }
    





}